import {
  createConnection,
  TextDocuments,
  Diagnostic,
  DiagnosticSeverity,
  ProposedFeatures,
  InitializeParams,
  DidChangeConfigurationNotification,
  CompletionItem,
  CompletionItemKind,
  TextDocumentPositionParams,
  SymbolInformation,
  WorkspaceSymbolParams,
  WorkspaceEdit,
  WorkspaceFolder,
  combineConsoleFeatures,
  TextDocumentSyncKind,
  Hover
} from 'vscode-languageserver';

import { TextDocument } from 'vscode-languageserver-textdocument';

import { HandlerResult } from 'vscode-jsonrpc';

import { configure, getLogger } from "log4js";
configure({
  appenders: {
    lsp_demo: {
      type: "dateFile",
      filename: "lsp_demo",
      pattern: "yyyy-MM-dd-hh.log",
      alwaysIncludePattern: true,
    },
  },
  categories: { default: { appenders: ["lsp_demo"], level: "debug" } }
});
const logger = getLogger("lsp_demo");

let connection = createConnection(ProposedFeatures.all);
let documents = new TextDocuments();

documents.listen(connection);

connection.onInitialize((params: InitializeParams) => {
  let capabilities = params.capabilities;
  return {
    capabilities: {
      hoverProvider: true,
      textDocumentSync: TextDocumentSyncKind.Incremental,
    }
  }; 
});

connection.onInitialized(() => {
  logger.info("Testing");
});

connection.listen();

connection.onDidSaveTextDocument(change => logger.info("Saved"))
