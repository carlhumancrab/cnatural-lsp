"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const vscode_languageserver_1 = require("vscode-languageserver");
const log4js_1 = require("log4js");
log4js_1.configure({
    appenders: {
        lsp_demo: {
            type: "dateFile",
            filename: "~/cn-logs/lsp_demo",
            pattern: "yyyy-MM-dd-hh.log",
            alwaysIncludePattern: true,
        },
    },
    categories: { default: { appenders: ["lsp_demo"], level: "debug" } }
});
const logger = log4js_1.getLogger("lsp_demo");
let connection = vscode_languageserver_1.createConnection(vscode_languageserver_1.ProposedFeatures.all);
let documents = new vscode_languageserver_1.TextDocuments();
documents.listen(connection);
connection.onInitialize((params) => {
    let capabilities = params.capabilities;
    return {
        capabilities: {
            hoverProvider: true,
            textDocumentSync: vscode_languageserver_1.TextDocumentSyncKind.Incremental,
        }
    };
});
connection.onInitialized(() => {
    logger.info("Testing");
});
connection.listen();
connection.onDidSaveTextDocument(change => logger.info("Saved"));
//# sourceMappingURL=server.js.map