"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.deactivate = exports.activate = void 0;
const path = require("path");
const vscode_languageclient_1 = require("vscode-languageclient");
let client;
function activate(context) {
    // Server side configurations
    let serverModule = context.asAbsolutePath(path.join('server', 'out', 'server.js'));
    let serverOptions = {
        module: serverModule, transport: vscode_languageclient_1.TransportKind.ipc
    };
    // Client side configurations
    let clientOptions = {
        // js is used to trigger things
        documentSelector: [{ scheme: 'file', language: 'js' }],
    };
    client = new vscode_languageclient_1.LanguageClient('cnatural-client', 'C Natural Client', serverOptions, clientOptions);
    // Start the client side, and at the same time also start the language server
    client.start();
}
exports.activate = activate;
function deactivate() {
    if (!client) {
        return undefined;
    }
    return client.stop();
}
exports.deactivate = deactivate;
//# sourceMappingURL=extension.js.map